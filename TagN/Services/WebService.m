//
//  WebService.m
//  TagN
//
//  Created by Kevin Lee on 2/4/16.
//  Copyright © 2016 Kevin Lee. All rights reserved.
//

#import "WebService.h"
#import <AFNetworking/AFNetworking.h>

@implementation WebService

static WebService *_webService = nil;
AFHTTPSessionManager *manager;

+ (WebService *) sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_webService == nil) {
            _webService = [[self alloc] init]; // assignment not done here
            
            manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:SERVER_URL]];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"image/jpeg", nil];
            
            if([GlobalService sharedInstance].user_me) {
                [manager.requestSerializer setValue:[GlobalService sharedInstance].user_me.user_access_token
                                 forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
            }
        }
    });
    
    return _webService;
}

- (NSString *)getErrorMessageFromNSError:(NSError *)error {
    
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    if(errorData && errorData.length > 0) {
        NSDictionary *dicError = [NSJSONSerialization JSONObjectWithData:errorData
                                                                 options:0
                                                                   error:nil];
        return dicError[SERVER_RESULT_MESSAGE];
    } else {
        return error.localizedDescription;
    }
}

- (NSInteger)getErrorCodeFromNSError:(NSError *)error {
    NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
    return response.statusCode;
}

- (void)loginWithUserEmail:(NSString *)user_email
                  UserPass:(NSString *)user_pass
                   success:(void (^)(UserMe *))success
                   failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_email":          user_email,
                                    @"user_pass":           user_pass,
                                    @"user_device_token":   [GlobalService sharedInstance].user_device_token,
                                    @"user_time_zone":      [[GlobalService sharedInstance] getUserTimeZone],
                                    @"user_device_type":    @"iOS"
                                    };
        
        [manager GET:@"/api/v1/users/login"
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 UserMe *objMe = [[UserMe alloc] initWithDictionary:responseObject];
                 [manager.requestSerializer setValue:objMe.user_access_token
                                  forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
                 success(objMe);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)fbloginWithName:(NSString *)user_name
               UserName:(NSString *)user_username
               UserPass:(NSString *)user_pass
              UserEmail:(NSString *)user_email
          UserAvatarUrl:(NSString *)user_avatar_url
              UserPhone:(NSString *)user_phone
                success:(void (^)(UserMe *))success
                failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_name":           user_name,
                                    @"user_username":       user_username,
                                    @"user_pass":           user_pass,
                                    @"user_email":          user_email,
                                    @"user_avatar_url":     user_avatar_url,
                                    @"user_phone":          user_phone,
                                    @"user_device_token":   [GlobalService sharedInstance].user_device_token,
                                    @"user_time_zone":      [[GlobalService sharedInstance] getUserTimeZone],
                                    @"user_device_type":    @"iOS"
                                    };
        
        [manager GET:@"/api/v1/users/fblogin"
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 UserMe *objMe = [[UserMe alloc] initWithDictionary:responseObject];
                 [manager.requestSerializer setValue:objMe.user_access_token
                                  forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
                 success(objMe);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)forgotPasswordWithUserEmail:(NSString *)user_email
                            success:(void (^)(NSString *))success
                            failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        
        NSDictionary *dicParams = @{
                                    @"user_email": user_email
                                    };
        [manager GET:@"/api/v1/users/forgotpassword"
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSDictionary *dicResult = (NSDictionary *)responseObject;
                 NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                 success(strResult);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)signupWithName:(NSString *)user_name
              UserName:(NSString *)user_username
             UserEmail:(NSString *)user_email
             UserPhone:(NSString *)user_phone
              UserPass:(NSString *)user_pass
            UserAvatar:(UIImage *)user_avatar
               success:(void (^)(UserMe *))success
               failure:(void (^)(NSString *))failure
              progress:(void (^)(double))progress {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_name":           user_name,
                                    @"user_username":       user_username,
                                    @"user_pass":           user_pass,
                                    @"user_email":          user_email,
                                    @"user_phone":          user_phone,
                                    @"user_device_token":   [GlobalService sharedInstance].user_device_token,
                                    @"user_time_zone":      [[GlobalService sharedInstance] getUserTimeZone],
                                    @"user_device_type":    @"iOS"
                                    };
        
        [manager POST:@"/api/v1/users"
           parameters:dicParams
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    if(user_avatar) {
        [formData appendPartWithFileData:[NSData dataWithData:UIImageJPEGRepresentation(user_avatar, 0.5f)]
                                    name:@"avatar"
                                fileName:@"avatar.jpg"
                                mimeType:@"image/jpeg"];
    }
}
             progress:^(NSProgress * _Nonnull uploadProgress) {
                 progress(uploadProgress.fractionCompleted);
             }
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  UserMe *objMe = [[UserMe alloc] initWithDictionary:responseObject];
                  [manager.requestSerializer setValue:objMe.user_access_token
                                   forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
                  success(objMe);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)logoutWithUserId:(NSNumber *)user_id
                 success:(void (^)(NSString *))success
                 failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager PATCH:[NSString stringWithFormat:@"/api/v1/users/%d", user_id.intValue]
            parameters:nil
               success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   NSDictionary *dicResult = (NSDictionary *)responseObject;
                   NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                   
                   [manager.requestSerializer setValue:nil
                                    forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
                   
                   success(strResult);
               }
               failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                   failure([self getErrorMessageFromNSError:error]);
               }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)updateUser:(UserMe *)objMe
        UserAvatar:(UIImage *)user_avatar
           success:(void (^)(UserMe *))success
           failure:(void (^)(NSString *))failure
          progress:(void (^)(double))progress {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_name":       objMe.user_name,
                                    @"user_username":   objMe.user_username,
                                    @"user_phone":      objMe.user_phone,
                                    @"user_avatar_url": objMe.user_avatar_url
                                    };
        
        [manager POST:[NSString stringWithFormat:@"/api/v1/users/%d", objMe.user_id.intValue]
           parameters:dicParams
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    if(user_avatar) {
        [formData appendPartWithFileData:[NSData dataWithData:UIImageJPEGRepresentation(user_avatar, 0.5f)]
                                    name:@"avatar"
                                fileName:@"avatar.jpg"
                                mimeType:@"image/jpeg"];
    }
}
             progress:^(NSProgress * _Nonnull uploadProgress) {
                 progress(uploadProgress.fractionCompleted);
             }
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  UserMe *objMe = [[UserMe alloc] initWithDictionary:responseObject];
                  success(objMe);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)changePassword:(NSNumber *)user_id
           CurrentPass:(NSString *)user_current_pass
               NewPass:(NSString *)user_new_pass
               success:(void (^)(NSString *))success
               failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_current_pass":   user_current_pass,
                                    @"user_new_pass":       user_new_pass
                                    };
        
        [manager PUT:[NSString stringWithFormat:@"/api/v1/users/%d", user_id.intValue]
          parameters:dicParams
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSDictionary *dicResult = (NSDictionary *)responseObject;
                 NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                 success(strResult);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)addTagWithUserId:(NSNumber *)tag_user_id
                    Text:(NSString *)tag_text
                 success:(void (^)(NSNumber *))success
                 failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"tag_user_id":     tag_user_id,
                                    @"tag_text":        tag_text
                                    };
        
        [manager POST:@"/api/v1/tags"
           parameters:dicParams
             progress:nil
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSDictionary *dicResult = (NSDictionary *)responseObject;
                  NSNumber *nResult = dicResult[SERVER_RESULT_MESSAGE];
                  success(nResult);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)deleteTagWithTagId:(NSNumber *)tag_id
                    UserId:(NSNumber *)user_id
                   success:(void (^)(NSString *))success
                   failure:(void (^)(NSString *))failure {
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id": user_id
                                    };
        [manager DELETE:[NSString stringWithFormat:@"/api/v1/tags/%d", tag_id.intValue]
             parameters:dicParams
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getTagImagesWithTagId:(NSNumber *)tag_id
                       UserId:(NSNumber *)user_id
                      success:(void (^)(ImageInfoObj *))success
                      failure:(void (^)(NSString *))failure {
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id":  user_id
                                    };
        
        [manager GET:[NSString stringWithFormat:@"/api/v1/tags/%d/images", tag_id.intValue]
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 ImageInfoObj *objImageInfo = [[ImageInfoObj alloc] initWithDictionary:responseObject];
                 success(objImageInfo);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)deleteImagesWithImageIds:(NSString *)image_ids
                          UserId:(NSNumber *)user_id
                        UserName:(NSString *)user_name
                         success:(void (^)(NSString *))success
                         failure:(void (^)(NSString *))failure {
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager.requestSerializer setValue:[GlobalService sharedInstance].user_access_token
                         forHTTPHeaderField:HTTP_HEADER_TOKEN_KEY];
        
        NSDictionary *dicParams = @{
                                    @"image_ids":           image_ids,
                                    @"image_user_id":       user_id,
                                    @"image_user_name":     user_name
                                    };
        [manager DELETE:@"/api/v1/images"
             parameters:dicParams
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getMyImagesWithUserId:(NSNumber *)user_id
                      PageNum:(NSInteger)page_num
                    PageCount:(NSInteger)page_count
                      success:(void (^)(NSArray *))success
                      failure:(void (^)(NSString *))failure {
    
    NSDictionary *dicParam = @{
                               @"page_num":     [NSNumber numberWithInteger:page_num],
                               @"page_count":   [NSNumber numberWithInteger:page_count]
                               };
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager GET:[NSString stringWithFormat:@"/api/v1/users/%d/images", user_id.intValue]
          parameters:dicParam
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryDicImageInfos = (NSArray *)responseObject;
                 NSMutableArray *aryImageInfos = [[NSMutableArray alloc] init];
                 for(NSDictionary *dicImageInfo in aryDicImageInfos) {
                     ImageInfoObj *objImageInfo = [[ImageInfoObj alloc] initWithDictionary:dicImageInfo];
                     [aryImageInfos addObject:objImageInfo];
                 }
                 success(aryImageInfos);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getShareImagesWithUserId:(NSNumber *)user_id
                         PageNum:(NSInteger)page_num
                       PageCount:(NSInteger)page_count
                         success:(void (^)(NSArray *))success
                         failure:(void (^)(NSString *))failure {
    
    NSDictionary *dicParam = @{
                               @"page_num":     [NSNumber numberWithInteger:page_num],
                               @"page_count":   [NSNumber numberWithInteger:page_count]
                               };
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager GET:[NSString stringWithFormat:@"/api/v1/users/%d/images/share", user_id.intValue]
          parameters:dicParam
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryDicImageInfos = (NSArray *)responseObject;
                 NSMutableArray *aryImageInfos = [[NSMutableArray alloc] init];
                 for(NSDictionary *dicImageInfo in aryDicImageInfos) {
                     ImageInfoObj *objImageInfo = [[ImageInfoObj alloc] initWithDictionary:dicImageInfo];
                     [aryImageInfos addObject:objImageInfo];
                 }
                 success(aryImageInfos);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)uploadImageObj:(ImageObj *)objImage
              WithData:(NSData *)dataImage
               success:(void (^)(NSNumber *))success
               failure:(void (^)(NSString *))failure {
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager POST:@"/api/v1/images"
           parameters:objImage.currentDictionary
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    [formData appendPartWithFileData:dataImage
                                name:@"image"
                            fileName:@"image.jpg"
                            mimeType:@"image/jpeg"];
}
             progress:nil
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSDictionary *dicResult = (NSDictionary *)responseObject;
                  NSNumber *nImageId = [NSNumber numberWithInt:[dicResult[SERVER_RESULT_MESSAGE] intValue]];
                  success(nImageId);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getTagUsersWithId:(NSNumber *)user_id
                    TagId:(NSNumber *)tag_id
                  success:(void (^)(NSArray *))success
                  failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager GET:[NSString stringWithFormat:@"/api/v1/users/%d/tags/%d", user_id.intValue, tag_id.intValue]
          parameters:nil
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryCategories = (NSArray *)responseObject;
                 
                 NSMutableArray *aryFriends = [[NSMutableArray alloc] init];
                 NSMutableArray *aryStrangers = [[NSMutableArray alloc] init];
                 
                 for(NSDictionary *dicUser in aryCategories[0]) {
                     UserObj *objUser = [[UserObj alloc] initWithDictionary:dicUser];
                     [aryFriends addObject:objUser];
                 }
                 
                 for(NSDictionary *dicUser in aryCategories[1]) {
                     UserObj *objUser = [[UserObj alloc] initWithDictionary:dicUser];
                     [aryStrangers addObject:objUser];
                 }
                 
                 success(@[aryFriends, aryStrangers]);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)sendShareRequestFrom:(NSNumber *)from_user_id
                FromUserName:(NSString *)from_user_name
                     ToUsers:(NSString *)to_user_ids
                       TagId:(NSNumber *)tag_id
                 TagFullText:(NSString *)tag_text
                     success:(void (^)(NSArray *))success
                     failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"share_from_user_id":      from_user_id,
                                    @"share_from_user_name":    from_user_name,
                                    @"share_to_user_ids":       to_user_ids,
                                    @"share_tag_id":            tag_id,
                                    @"share_tag_text":          tag_text
                                    };
        
        [manager POST:@"/api/v1/shares"
           parameters:dicParams
             progress:nil
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSArray *aryShareIds = (NSArray *)responseObject;
                  success(aryShareIds);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)sendUnshareRequestFrom:(NSNumber *)from_user_id
                  FromUserName:(NSString *)from_user_name
                            To:(NSNumber *)to_user_id
                       ShareId:(NSNumber *)share_id
                   TagFullText:(NSString *)tag_text
                       success:(void (^)(NSString *))success
                       failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"sender_id":       from_user_id,
                                    @"sender_user_name":from_user_name,
                                    @"receiver_id":     to_user_id,
                                    @"tag_text":        tag_text
                                    };
        
        [manager DELETE:[NSString stringWithFormat:@"/api/v1/shares/%d", share_id.intValue]
             parameters:dicParams
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)sendUnshareMe:(NSNumber *)user_id
              FromTag:(NSNumber *)tag_id
              success:(void (^)(NSString *))success
              failure:(void (^)(NSString *))failure {
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager DELETE:[NSString stringWithFormat:@"/api/v1/users/%d/tags/%d", user_id.intValue, tag_id.intValue]
             parameters:nil
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getAllNotificationsWithUserId:(NSNumber *)user_id
                              success:(void (^)(NSArray *))success
                              failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager GET:[NSString stringWithFormat:@"/api/v1/users/%d/notifications", user_id.intValue]
          parameters:nil
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryDicNotis = (NSArray *)responseObject;
                 NSMutableArray *aryNotis = [[NSMutableArray alloc] init];
                 for(NSDictionary *dicNoti in aryDicNotis) {
                     NotiObj *objNoti = [[NotiObj alloc] initWithDictionary:dicNoti];
                     [aryNotis addObject:objNoti];
                 }
                 
                 success(aryNotis);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)markAllNotificationsAsRead:(NSNumber *)user_id
                           success:(void (^)(NSString *))success
                           failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager PATCH:[NSString stringWithFormat:@"/api/v1/users/%d/notifications", user_id.intValue]
            parameters:nil
               success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   NSDictionary *dicResult = (NSDictionary *)responseObject;
                   NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                   success(strResult);
               }
               failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                   failure([self getErrorMessageFromNSError:error]);
               }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)sendShareResponseWithId:(NSNumber *)share_id
                         Status:(TAG_USER_STATUS)share_status
                   FromUserName:(NSString *)sender_user_name
                        success:(void (^)(NSString *))success
                        failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"sender_user_name":    sender_user_name,
                                    @"share_status":        [NSNumber numberWithInt:share_status]
                                    };
        
        [manager PATCH:[NSString stringWithFormat:@"/api/v1/shares/%d", share_id.intValue]
            parameters:dicParams
               success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   NSDictionary *dicResult = (NSDictionary *)responseObject;
                   NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                   success(strResult);
               }
               failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                   failure([self getErrorMessageFromNSError:error]);
               }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getNotificationWithId:(NSNumber *)noti_id
                      success:(void (^)(NotiObj *))success
                      failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager GET:[NSString stringWithFormat:@"/api/v1/notifications/%d", noti_id.intValue]
          parameters:nil
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSDictionary *dicResult = (NSDictionary *)responseObject;
                 
                 NotiObj *objNoti = [[NotiObj alloc] initWithDictionary:dicResult];
                 
                 success(objNoti);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)removeNotificationWithId:(NSNumber *)noti_id
                         success:(void (^)(NSString *))success
                         failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager DELETE:[NSString stringWithFormat:@"/api/v1/notifications/%d", noti_id.intValue]
             parameters:nil
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

#pragma mark images likes
- (void)updateUserLikeWithId:(NSNumber *)user_id
                    UserName:(NSString *)user_name
                     ImageId:(NSNumber *)image_id
                  ImageTagId:(NSNumber *)image_tag_id
                    Selected:(BOOL)is_like
                     success:(void (^)(NSString *))success
                     failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id":     user_id,
                                    @"user_name":   user_name,
                                    @"image_tag_id":image_tag_id,
                                    @"is_like":     [NSNumber numberWithBool:is_like]
                                    };
        
        [manager POST:[NSString stringWithFormat:@"/api/v1/images/%d/likes", image_id.intValue]
           parameters:dicParams
             progress:nil
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSDictionary *dicResult = (NSDictionary *)responseObject;
                  NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                  success(strResult);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

#pragma mark comment

- (void)getImageComments:(NSNumber *)image_id
                  UserId:(NSNumber *)user_id
                 success:(void (^)(NSArray *))success
                 failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id": user_id
                                    };
        
        [manager GET:[NSString stringWithFormat:@"/api/v1/images/%d/comments", image_id.intValue]
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryDicComments = (NSArray *)responseObject;
                 NSMutableArray *aryComments = [[NSMutableArray alloc] init];
                 
                 for(NSDictionary *dicComment in aryDicComments) {
                     CommentObj *objComment = [[CommentObj alloc] initWithDictionary:dicComment];
                     [aryComments addObject:objComment];
                 }
                 
                 success(aryComments);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getImageLikers:(NSNumber *)image_id
                UserId:(NSNumber *)user_id
               success:(void (^)(NSArray *))success
               failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id": user_id
                                    };
        
        [manager GET:[NSString stringWithFormat:@"/api/v1/images/%d/likers", image_id.intValue]
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSArray *aryDicLikers = (NSArray *)responseObject;
                 NSMutableArray *aryImageLikers = [[NSMutableArray alloc] init];
                 for(NSDictionary *dicLiker in aryDicLikers) {
                     ImageLikerObj *objLiker = [[ImageLikerObj alloc] initWithDictionary:dicLiker];
                     [aryImageLikers addObject:objLiker];
                 }
                 
                 success(aryImageLikers);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)getImageWithImageId:(NSNumber *)image_id
                     UserId:(NSNumber *)user_id
                    success:(void (^)(ImageObj *))success
                    failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"user_id": user_id
                                    };
        
        [manager GET:[NSString stringWithFormat:@"/api/v1/images/%d", image_id.intValue]
          parameters:dicParams
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSDictionary *dicResult = (NSDictionary *)responseObject;
                 
                 ImageObj *objImage = [[ImageObj alloc] initWithDictionary:dicResult];
                 success(objImage);
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure([self getErrorMessageFromNSError:error]);
             }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)addCommentWithUserId:(NSNumber *)comment_user_id
                     ImageId:(NSNumber *)comment_image_id
                      String:(NSString *)comment_string
                     success:(void (^)(CommentObj *))success
                     failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        NSDictionary *dicParams = @{
                                    @"comment_user_id":     comment_user_id,
                                    @"comment_image_id":    comment_image_id,
                                    @"comment_string":      comment_string
                                    };
        
        [manager POST:@"/api/v1/comments"
           parameters:dicParams
             progress:nil
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSDictionary *dicResult = (NSDictionary *)responseObject;
                  
                  CommentObj *objComment = [[CommentObj alloc] initWithDictionary:dicResult];
                  success(objComment);
              }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failure([self getErrorMessageFromNSError:error]);
              }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

- (void)removeCommentWithCommentId:(NSNumber *)comment_id
                           success:(void (^)(NSString *))success
                           failure:(void (^)(NSString *))failure {
    
    if([GlobalService sharedInstance].is_internet_alive) {
        [manager DELETE:[NSString stringWithFormat:@"/api/v1/comments/%d", comment_id.intValue]
             parameters:nil
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dicResult = (NSDictionary *)responseObject;
                    NSString *strResult = dicResult[SERVER_RESULT_MESSAGE];
                    success(strResult);
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    failure([self getErrorMessageFromNSError:error]);
                }];
    } else {
        failure(JDSTATUS_NOTIFICATION_NO_INTERNET);
    }
}

@end
